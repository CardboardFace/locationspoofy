#include "PosHandler.h"

@implementation PosHandler
    +(CLLocationCoordinate2D)getSavedPos {
		NSMutableDictionary* posSettings = [NSMutableDictionary dictionaryWithContentsOfFile:kPosPrefPath];
        CLLocationCoordinate2D spoofedPos;

        // Return 0,0 if nothing saved
        if (posSettings == nil) {
            spoofedPos.latitude = 0;
            spoofedPos.longitude = 0;
        } else {
            RLog(@"Saved pos: %@", posSettings);

            spoofedPos.longitude = 	[([posSettings valueForKey:@"Longitude"] ?: 0) doubleValue];
            spoofedPos.latitude = 	[([posSettings valueForKey:@"Latitude"] ?: 0) doubleValue];
        }
        
        return spoofedPos;
    }

    +(void)savePos:(CLLocationCoordinate2D)newPos {
        NSMutableDictionary* posSettings = [NSMutableDictionary dictionaryWithContentsOfFile:kPosPrefPath];
        if (posSettings == nil) posSettings = [[NSMutableDictionary alloc] init];
        RLog(@"Pos2 saving: %@", posSettings);

        // Update settings dictionary
        [posSettings setObject:[NSNumber numberWithDouble:(double)newPos.longitude] forKey:@"Longitude"];
        [posSettings setObject:[NSNumber numberWithDouble:(double)newPos.latitude] forKey:@"Latitude"];
        
        RLog(@"Pos settings: %@", posSettings);

        // Write to plist (in current thread to stop redrops causing conflicting saves while saving)
        bool success = [posSettings writeToFile:kPosPrefPath atomically:YES];
        if (!success) RLog(@"SAVE FAILURE AT PATH: %@", kPosPrefPath);
    }
@end
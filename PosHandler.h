#import <MapKit/MapKit.h>
#include <RemoteLog.h>
#define kPosPrefPath @"/var/mobile/Library/Preferences/com.cardboardface.locationspoofy.pos.plist" // Path for tweak preferences file

@interface PosHandler : NSObject
    +(CLLocationCoordinate2D)getSavedPos;
    +(void)savePos:(CLLocationCoordinate2D)newPos;
@end
// Made by CardboardFace
// CLLocation header: https://developer.limneos.net/index.php?ios=12.1&framework=CoreLocation.framework&header=CLLocation.h
#import <MapKit/MapKit.h>
#include <RemoteLog.h>
#include "PosHandler.h"
#define kPreferencePath @"/var/mobile/Library/Preferences/com.cardboardface.locationspoofy.prefs.plist" // Path for tweak preferences file

/* Dependencies for respring button */
@interface NSTask : NSObject
    @property (copy) NSString *launchPath;

    - (id)init;
    - (void)launch;
@end

// Preferences
static NSMutableDictionary *settings;
static BOOL tweakEnabled;

// Returns true if the currently hooked process is the SpringBoard
static BOOL isOnSpringBoard() {
	return [[NSBundle mainBundle].bundleIdentifier isEqual:@"com.apple.springboard"];
}

// Load preference updates into variables
static void refreshPrefs() {

	// Ensure preferences file has been created (if not load, load default preferences from plist defaults)
	CFArrayRef keyList = CFPreferencesCopyKeyList(CFSTR("com.cardboardface.locationspoofy.prefs"), kCFPreferencesCurrentUser, kCFPreferencesAnyHost);
	if (keyList) {
		settings = (NSMutableDictionary *)CFBridgingRelease(CFPreferencesCopyMultiple(keyList, CFSTR("com.cardboardface.locationspoofy.prefs"), kCFPreferencesCurrentUser, kCFPreferencesAnyHost));
		CFRelease(keyList);
	} else {
		settings = nil;
	}
	if (!settings) {
		settings = [NSMutableDictionary dictionaryWithContentsOfFile:kPreferencePath];
	}

	// Load settings into vars (loading a default value if no setting is set)
	tweakEnabled = [([settings objectForKey:@"Enabled"] ?: @(NO)) boolValue];
}
static void PreferencesChangedCallback(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo) {
	refreshPrefs();

	// Ask user if they want to respring (only from SpringBoard to avoid multiple popups)
	if (!isOnSpringBoard()) return;
	UIAlertController *alertController = [UIAlertController
		alertControllerWithTitle:@"Respring?"
		message:@"A respring is required to apply this for some open apps"
		preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction *okAction = [UIAlertAction
		actionWithTitle:NSLocalizedString(@"Respring", @"OK action")
		style:UIAlertActionStyleDestructive
		handler:^(UIAlertAction *action)
			{
				// Respring:
				NSTask *task = [[NSTask alloc] init];
				[task setLaunchPath:@"/usr/bin/sbreload"];
				[task launch];
			}];
	UIAlertAction *laterAction = [UIAlertAction
		actionWithTitle:NSLocalizedString(@"Later", @"Later action")
		style:UIAlertActionStyleCancel
		handler:^(UIAlertAction *action) { } ];

	[alertController addAction:laterAction];
	[alertController addAction:okAction];
	[[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alertController animated:YES completion:nil];
}

%hook CLLocation
	-(CLLocationCoordinate2D)coordinate {
		if (!tweakEnabled) return %orig;
		return [PosHandler getSavedPos];
	}
%end


%ctor {
	// Add observer to call PreferencesChangedCallback() when the preferences are updated
	CFNotificationCenterAddObserver(
			CFNotificationCenterGetDarwinNotifyCenter(),
			NULL,
			(CFNotificationCallback) PreferencesChangedCallback,
			CFSTR("com.cardboardface.locationspoofy.prefs.prefschanged"),
			NULL,
			CFNotificationSuspensionBehaviorCoalesce);

	refreshPrefs();

	%init;
}
#import <Preferences/PSListController.h>
#import <Preferences/PSSwitchTableCell.h>
#import <MapKit/MapKit.h>
#include "../PosHandler.h"

@interface LocationSpoofyListController : PSListController
@end

@interface LocationSpoofyMapView : UITableViewCell {
		UILabel* arghLabel;
		MKMapView* mapView;
		MKPointAnnotation* spoofPoint;
	};
	@property CGFloat mapHeight;
	@property CGFloat mapWidth;
	@property int defaultZoomLevel;
	@property CLLocationCoordinate2D spoofedPos;
	-(void)createPin;
@end
#include "LocationSpoofyListController.h"

@implementation LocationSpoofyListController
	-(NSArray *)specifiers {
		if (!_specifiers) {
			_specifiers = [[self loadSpecifiersFromPlistName:@"Root" target:self] retain];
		}
		return _specifiers;
	}

	// Opens my Twitter page
	- (void)twitterlinkcardboard {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/face_cardboard"] options:@{} completionHandler:nil];
	}
@end

// Function to play haptic feedback
static void PlayHaptic() {
	UISelectionFeedbackGenerator* feedbackGenerator = [[UISelectionFeedbackGenerator alloc] init];
	[feedbackGenerator prepare]; // Prepare before playing
	[feedbackGenerator selectionChanged]; // Play haptic
}



@implementation LocationSpoofyMapView
	@synthesize mapWidth, mapHeight, defaultZoomLevel, spoofedPos;

	-(id)initWithSpecifier:(PSSpecifier *)specifier {
		self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
		if (self) {
			// Map settings
			mapWidth = [[UIApplication sharedApplication] keyWindow].frame.size.width;
			mapHeight = [[UIApplication sharedApplication] keyWindow].frame.size.height * 0.5;
			defaultZoomLevel = 1000000;
			
			mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, mapWidth, mapHeight)];
			[self addSubview:mapView];

			// Get saved pos
			spoofedPos = [PosHandler getSavedPos];

			// Drop pin on user's saved location
			spoofPoint = [[MKPointAnnotation new] init];
			[self createPin];

			// Add tap-hold gesture to change location
			mapView.userInteractionEnabled = YES;
			UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer new] initWithTarget:self action:@selector(handleSpoofPos:)];
			lpgr.minimumPressDuration = 0.5; // Seconds to hold
			[mapView addGestureRecognizer:lpgr];
		}
		return self;
	}
	-(CGFloat)preferredHeightForWidth:(CGFloat)arg1 {
		return mapHeight;
	}


	-(void)createPin {
		// Set location and zoom level
		MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(spoofedPos, defaultZoomLevel, defaultZoomLevel);
		MKCoordinateRegion adjustedRegion = [mapView regionThatFits:viewRegion];
		[mapView setRegion:adjustedRegion animated:YES];

		// Set annotation to point at your coordinate
		spoofPoint.coordinate = spoofedPos;
		spoofPoint.title = @"Spoofed location";

		//Drop pin on map
		[mapView addAnnotation:spoofPoint];
	}

	-(void)handleSpoofPos:(UIGestureRecognizer *)gestureRecognizer {
		
		// Play haptic on start/end
		if (gestureRecognizer.state == UIGestureRecognizerStateEnded || gestureRecognizer.state == UIGestureRecognizerStateBegan) {
			PlayHaptic();
		} else if (gestureRecognizer.state != UIGestureRecognizerStateChanged) {
			return;
		}
		
		// Move pin while pos is changing
		CGPoint touchPoint = [gestureRecognizer locationInView:mapView];
		CLLocationCoordinate2D touchMapCoordinate = [mapView convertPoint:touchPoint toCoordinateFromView:mapView];
		spoofPoint.coordinate = touchMapCoordinate;

		// Save to plist on pin drop
		if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
			[PosHandler savePos:touchMapCoordinate];
		}
	}
@end
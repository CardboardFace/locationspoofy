# LocationSpoofy
A lightweight jailbreak tweak to hack your GPS location using a Map view in a settings panel.

Simply tap and hold to move your virtual location.


## Preview
<img src="https://gitlab.com/CardboardFace/locationspoofy/-/raw/master/Art/Preview.PNG" width="220">